<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180509_154555_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => 'pk',
            'password'=>'string NOT NULL',
            'email'=>'string NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
