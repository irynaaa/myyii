<?php
/* @var $this yii\web\View */
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use mihaildev\ckeditor;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
$this->registerMetaTag(['name' => 'keywords', 'content' => 'yii, developing, views,
      meta, tags']);
$this->registerMetaTag(['name' => 'description',
    'content' => 'This is the description of this page!'], 'description');
?>

<script src="../../vendor/mihaildev/yii2-ckeditor/editor/ckeditor.js"/>
<div class="site-about">
    <form>
            <textarea name="editor1" id="editor1" rows="10" cols="80">
                This is my textarea to be replaced with CKEditor.
            </textarea>
        <script>
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace( 'editor1' );
        </script>
    </form>
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <h1><?= HelloWorld\SayHello::world();  ?></h1>
    <?php
    echo DateTimePicker::widget([
        'name' => 'dp_1',
        'type' => DateTimePicker::TYPE_INPUT,
        'value' => '23-Feb-1982 10:10',
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-M-yyyy hh:ii'
        ]
    ]);
    ?>
</div>

