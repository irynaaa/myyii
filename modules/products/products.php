<?php
namespace app\modules\products;
use Yii;

class Products extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations['products'])) {
            Yii::$app->i18n->translations['products'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'sourceLanguage' => 'en',
                'basePath' => '@app/modules/products/messages'
            ];
        }
    }
}
?>
