<?php

use yii\db\Migration;

/**
 * Class m180509_171011_seed_data_user
 */
class m180509_171011_seed_data_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('user',array(
            'email'=>'test1@notanaddress.com',
            'password' => md5('test1'),
        ));
        $this->insert('user',array(
            'email'=>'test2@notanaddress.com',
            'password' => md5('test2'),
        ));
        $this->insert('user',array(
            'email'=>'test3@notanaddress.com',
            'password' => md5('test3'),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['email' => 'test1@notanaddress.com']);
        $this->delete('user', ['email' => 'test2@notanaddress.com']);
        $this->delete('user', ['email' => 'test3@notanaddress.com']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180509_171011_seed_data_user cannot be reverted.\n";

        return false;
    }
    */
}
