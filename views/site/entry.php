<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'name')->label('Ім\'я') ?>

<?= $form->field($model, 'email')->label('Пошта') ?>

<?= $form->field($model, 'password')->label('Пароль') ?>

<?= $form->field($model, 'password_repeat')->label('Підтвердіть пароль') ?>

    <div class="form-group">
<?= Html::submitButton('Відправити', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>