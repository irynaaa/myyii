<?php

namespace app\modules\products\models\Product;
use app\modules\products\models\Category\Category;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $photo
 * @property int $count
 * @property int $category_id
 *
 * @property Category $category
 */
class Product extends \yii\db\ActiveRecord
{
    public $photo_upload;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count', 'category_id'], 'integer'],
            [['title', 'description', 'photo'], 'string', 'max' => 255],
            [['title', 'description'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['photo_upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['photo_upload'], 'safe'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->photo_upload->saveAs('photo_uploads/' . $this->photo_upload->baseName . '.' . $this->photo_upload->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'photo' => 'Photo',
            'count' => 'Count',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
