<?php

use yii\db\Migration;

/**
 * Class m180509_180633_update_seed_data_username
 */
class m180509_180633_update_seed_data_username extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('user',array(
            'username'=>'john'),
            'id=1'
            //'email =test1@notanaddress.com'//хотіла по email запдейтити,
                                            // але чомусь не вийшло, тільки по id пройшла міграція
        );
        $this->update('user',array(
            'username'=>'tom'),
            'id=2'
            //'email =test2@notanaddress.com'
        );
        $this->update('user',array(
            'username'=>'sam'),
            'id=3'
            //'email =test3@notanaddress.com'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180509_180633_update_seed_data_username cannot be reverted.\n";

        return false;
    }
    */
}
