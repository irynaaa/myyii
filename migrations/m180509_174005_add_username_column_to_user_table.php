<?php

use yii\db\Migration;

/**
 * Handles adding username to table `user`.
 */
class m180509_174005_add_username_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'username', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'username');
    }
}
